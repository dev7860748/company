import { Injectable, OnDestroy } from '@angular/core';
import { Subject } from "rxjs";

@Injectable()
export abstract class Unsubscribable implements OnDestroy {

  protected _destroy$: Subject<void> = new Subject<void>();

  ngOnDestroy() {
    this._destroy$.next();
    this._destroy$.complete();
  }
}
