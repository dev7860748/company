import { Pipe, PipeTransform } from '@angular/core';
import { ClientStatus } from '../../models/client';

@Pipe({
  name: 'clientStatus',
  pure: true,
})
export class ClientStatusPipe implements PipeTransform {

  transform(status: ClientStatus): string {
    switch (status) {
      case ClientStatus.CLIENT:
        return 'Client'
      case ClientStatus.DEMO:
        return 'Demo'
      case ClientStatus.Lead:
        return 'Lead'
      default:
        return 'NA';
    }
  }

}
