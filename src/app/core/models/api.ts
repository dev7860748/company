import { InjectionToken, Provider } from "@angular/core";

export interface APIInterface {
  client: string;
}

export const API = new InjectionToken<APIInterface>('API Paths');

export const API_PROVIDER: Provider = {
  provide: API,
  useValue: {
    client: '/client'
  }
}