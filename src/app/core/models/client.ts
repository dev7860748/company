export interface Client {
  first_name?: string;
  last_name?: string;
  email?: string;
  birth_date?: number;
  registration_date?: number;
  ip_address?: string;
  status?: ClientStatus;
}

export enum ClientStatus {
  Lead = "L",
  DEMO = "D",
  CLIENT = "C"
}