import { HttpClient } from '@angular/common/http';
import { Inject, Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { API, APIInterface } from '../models/api';
import { Client, ClientStatus } from '../models/client';

@Injectable({
  providedIn: 'root'
})
export class ClientService {

  constructor(
    private _http: HttpClient,
    @Inject(API) private _API: APIInterface
  ) { }

  loadClients(params?: { q?: string, status?: string, birth_date?: number }): Observable<Client[]> {
    return this._http.get<Client[]>(environment.baseURL + this._API.client, {
      params: params
    });
  }

  addClient(client: Client): Observable<{ message: string }> {
    return this._http.post<{ message: string }>(environment.baseURL + this._API.client, client);
  }
}
