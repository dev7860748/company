import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { Client } from 'src/app/core/models/client';

@Component({
  selector: 'app-clients-list',
  templateUrl: './clients-list.component.html',
  styleUrls: ['./clients-list.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ClientsListComponent {
  @Input() clients: Client[] | null = [];

  displayedColumns: string[] = [
    'first_name',
    'last_name',
    'email',
    'birth_date',
    'registration_date',
    'ip_address',
    'status',
  ];
}
