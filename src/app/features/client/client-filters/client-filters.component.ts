import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { MatCheckboxChange } from '@angular/material/checkbox';
import { ClientStatus } from 'src/app/core/models/client';

@Component({
  selector: 'app-client-filters',
  templateUrl: './client-filters.component.html',
  styleUrls: ['./client-filters.component.scss']
})
export class ClientFiltersComponent implements OnInit {
  @Output() formInitialized = new EventEmitter<FormGroup<FiltersForm>>;

  filtersForm!: FormGroup<FiltersForm>;

  maxDate = new Date();

  public statusCheckboxOptions: ClientStatus[] = [ClientStatus.CLIENT, ClientStatus.DEMO, ClientStatus.Lead];

  constructor(private _formBuilder: FormBuilder) { }

  ngOnInit(): void {
    this._initializeForm();
  }

  private _initializeForm() {
    this.filtersForm = this._formBuilder.group({
      q: [''],
      status: this._formBuilder.array(this.statusCheckboxOptions.map((option) => this._formBuilder.control(option))),
      birth_date: null
    });

    this.formInitialized.emit(this.filtersForm);
  }

  handleCheckboxChange(event: MatCheckboxChange, value: string, index: number) {
    const statusControl = this.filtersForm.get('status') as FormArray;
    if (statusControl) {
      statusControl.controls[index].patchValue(event.checked ? value : '');
    }
  }
}

export interface FiltersForm {
  q: FormControl;
  status: FormArray;
  birth_date: FormControl;
}
