import { Component } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { MatDialog } from "@angular/material/dialog";
import { debounceTime, startWith, switchMap, take, takeUntil } from 'rxjs';
import { Unsubscribable } from 'src/app/core/helpers/unsubscribable';
import { Client, ClientStatus } from 'src/app/core/models/client';
import { ClientService } from 'src/app/core/services/client.service';
import { AddClientComponent } from '../add-client-modal/add-client.component';
import { FiltersForm } from '../client-filters/client-filters.component';

@Component({
  selector: 'app-clients-page',
  templateUrl: './clients-page.component.html',
  styleUrls: ['./clients-page.component.scss']
})
export class ClientsPageComponent extends Unsubscribable {
  clients: Client[] = [];

  filtersForm!: FormGroup<FiltersForm>;

  loadingData = true;

  error = '';

  public statusCheckboxOptions: ClientStatus[] = [ClientStatus.CLIENT, ClientStatus.DEMO, ClientStatus.Lead];

  constructor(
    private _clientsService: ClientService,
    private _dialog: MatDialog,
  ) {
    super();
  }

  listenToFiltersForm(form: FormGroup) {
    this.filtersForm = form;

    form.valueChanges.pipe(
      startWith(form.value),
      debounceTime(400),
      switchMap(() => this._getClients(form)),
      takeUntil(this._destroy$)
    ).subscribe({
      next: (res) => {
        this.clients = res;
        this.loadingData = false;
      },
      error: (e) => {
        this.error = e.message;
        this.loadingData = false;
      }
    });
  }

  private _getClients(filtersForm: FormGroup<FiltersForm>) {
    this.loadingData = true;
    this.error = '';
    return this._clientsService.loadClients({
      q: filtersForm.value.q ?? '',
      status: filtersForm.value.status.filter((value: string) => value).join(',') ?? '',
      birth_date: filtersForm.value.birth_date?.getTime() ?? ''
    });
  }

  openDialog() {
    this._dialog.open(AddClientComponent, {
      width: '450px'
    });

    this._dialog.afterAllClosed.pipe(take(1)).subscribe({
      next: () => {
        this.filtersForm.patchValue({});
      }
    })
  }
}
