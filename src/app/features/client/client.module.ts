import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { MatButtonModule } from "@angular/material/button";
import { MatCheckboxModule } from "@angular/material/checkbox";
import { MatNativeDateModule } from '@angular/material/core';
import { MatDatepickerModule } from "@angular/material/datepicker";
import { MatDialogModule } from "@angular/material/dialog";
import { MatFormFieldModule } from "@angular/material/form-field";
import { MatIconModule } from "@angular/material/icon";
import { MatInputModule } from "@angular/material/input";
import { MatProgressSpinnerModule } from "@angular/material/progress-spinner";
import { MatRadioModule } from "@angular/material/radio";
import { MatTableModule } from "@angular/material/table";
import { ClientStatusPipeModule } from 'src/app/core/pipes/client-status/client-status-pipe.module';
import { AddClientComponent } from './add-client-modal/add-client.component';
import { ClientRoutingModule } from './client-routing.module';
import { ClientsListComponent } from './clients-list/clients-list.component';
import { ClientsPageComponent } from './clients-page/clients-page.component';
import { ClientFiltersComponent } from './client-filters/client-filters.component';


@NgModule({
  declarations: [
    ClientsPageComponent,
    ClientsListComponent,
    AddClientComponent,
    ClientFiltersComponent
  ],
  imports: [
    CommonModule,
    ClientRoutingModule,
    MatDialogModule,
    MatButtonModule,
    MatInputModule,
    MatFormFieldModule,
    MatIconModule,
    FormsModule,
    ReactiveFormsModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatRadioModule,
    MatCheckboxModule,
    MatTableModule,
    ClientStatusPipeModule,
    MatProgressSpinnerModule,
  ],
  exports: [
    ClientsPageComponent,
    ClientsListComponent,
    AddClientComponent
  ]
})
export class ClientModule { }
