import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef } from '@angular/material/dialog';
import { take } from 'rxjs';
import { Regex } from 'src/app/core/helpers/regex';
import { ClientStatus } from 'src/app/core/models/client';
import { ClientService } from 'src/app/core/services/client.service';

@Component({
  selector: 'app-add-client',
  templateUrl: './add-client.component.html',
  styleUrls: ['./add-client.component.scss']
})
export class AddClientComponent implements OnInit {
  clientForm!: FormGroup<{ first_name: FormControl; last_name: FormControl; email: FormControl; birth_date: FormControl; ip_address: FormControl; status: FormControl }>;

  saving = false;

  maxDate = new Date();

  error = '';

  constructor(
    private _formBuilder: FormBuilder,
    private _clientService: ClientService,
    private _dialogRef: MatDialogRef<AddClientComponent>
  ) { }

  ngOnInit(): void {
    this.clientForm = this._formBuilder.group({
      first_name: ["", Validators.required],
      last_name: ["", Validators.required],
      email: ["", [Validators.required, Validators.email]],
      birth_date: [null, Validators.required],
      ip_address: ["", [Validators.required, Validators.pattern(Regex.IPADDRESS)]],
      status: ["L", Validators.required],
    })
  }

  save() {
    this.error = '';
    this.saving = true;

    let clientData = {
      first_name: this.clientForm.value.first_name,
      last_name: this.clientForm.value.last_name,
      email: this.clientForm.value.email,
      birth_date: this.clientForm.value.birth_date.getTime(),
      ip_address: this.clientForm.value.ip_address,
      status: this.clientForm.value.status as ClientStatus,
    };

    this._clientService.addClient(clientData).pipe(take(1)).subscribe({
      next: () => {
        this.saving = false;
        this.close();
      },
      error: (e) => {
        this.error = e.message;
        this.saving = false;
      }
    });
  }

  close() {
    this._dialogRef.close(true);
  }
}
